import { Injectable } from '@angular/core';

export class CounterService {

  private _activeToInactiveCount = 0;
  private _inactiveToActiveCount = 0;

  get activeToInactiveCount() {
    return this._activeToInactiveCount;
  }

  get inactiveToActiveCount() {
    return this._inactiveToActiveCount;
  }

  countActiveToInactive() {
    this._activeToInactiveCount++;
  }

  countInactiveToActive() {
    this._inactiveToActiveCount++;
  }
}
